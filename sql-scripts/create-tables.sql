CREATE TABLE sows(
    id integer auto_increment,
    dealId integer,
    customer varchar(50),
    invoiceEmail varchar(50),
    userEmail varchar(50),
    psaId varchar(10),
    psaDate varchar(30),
    startDate varchar(30),
    endDate varchar(30),
    customerAddress varchar(100),
    performanceAddress varchar(100),
    totalResources integer,
    primary key (id)
);

CREATE TABLE sow_res(
    sowId integer,
    resourceId integer,
    quantity integer,
    primary key (sowId, resourceId)
);

CREATE TABLE resources(
    id integer auto_increment,
    rate integer,
    description varchar(150),
    primary key (id)
);

CREATE TABLE res_cat(
    categoryId integer,
    resourceId integer,
    primary key (categoryId,resourceId)
);

CREATE TABLE categories(
    categoryName varchar(50),
    id integer auto_increment,
    primary key (id)
);

CREATE TABLE sow_tas(
    sowId integer,
    taskId integer,
    primary key (sowId,taskId)
);

CREATE TABLE tasks(
    id integer auto_increment,
    taskTemplate integer,
    primary key (id)
);