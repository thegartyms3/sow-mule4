import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MongoConnector {

    private static MongoClient connect(String fullUri){

        MongoClientURI uri = new MongoClientURI(fullUri);
        MongoClient mclient = new MongoClient(uri);
        return mclient;

    }

    public static List<Document> getAllElements(String db, String uri, String collection) {
        MongoClient mongocl = connect(uri);
        MongoCollection<Document> mongoC = mongocl.getDatabase(db).getCollection(collection);
        return (List<Document>)mongoC.find().into(new ArrayList<Document>());
    }
    
    public static List<Document> getElementsByString(String db, String uri, String collection, String search, String field) {
        MongoClient mongocl = connect(uri);
    	MongoCollection<Document> mongoC = mongocl.getDatabase(db).getCollection(collection);
        
    	BasicDBObject where = new BasicDBObject();
        where.put(field, search);
        
        List<Document> toReturn = (List<Document>)mongoC.find(where).into(new ArrayList<Document>());
        mongocl.close();
        
        return toReturn;
    }

    public static void postElements(String db, String uri, String collection, List<Document> toPost) {
        MongoClient mongocl = connect(uri);
        MongoCollection<Document> mongoC = mongocl.getDatabase(db).getCollection(collection);
        
        mongoC.insertMany(toPost);
        mongocl.close();
    }

    public static void deleteElement(String db, String uri, String collection, String searchBy, String field) {
    	MongoClient mongocl = connect(uri);
    	MongoCollection<Document> mongoC = mongocl.getDatabase(db).getCollection(collection);
    	
    	mongoC.deleteOne(Filters.eq(field,searchBy));
    	mongocl.close();
    }

    public static void updateElement(String db, String uri, String collection, LinkedHashMap updatedInfo, String searchBy, String field) {
        MongoClient mongocl = connect(uri);
        MongoCollection<Document> mongoC = mongocl.getDatabase(db).getCollection(collection);
        mongoC.replaceOne(Filters.eq(field,searchBy), new Document(updatedInfo));
        mongocl.close();
    }

}
